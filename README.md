# Stex Deconverter

Bash script to mass de-convert .stex files back into .png files.

It's a bit of a hack (and specifically made for my own particular use-case) and while it worked for me, I have no idea if it will always work.

The basic gist is that it removes the first 32 bytes of each file because apparently that's the only difference between a .png file and a .stex file, just some extra bit of header.
