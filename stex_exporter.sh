#!/bin/bash

echo "--- Starting 'STEX MASS EXPORTER' script ---" >&2

echo "This will export `ls *.stex | wc -l` '.stex' files found in the current folder into a new folder called 'exported', and rename them." >&2

read -p "Are you sure? " -n 1 -r
echo 
if [[ $REPLY =~ ^[Yy]$ ]]
then
    mkdir -p exported
    for file in *.stex
        do
        if [ -f $file ]
        then
            tail +33c "$file" > "$file".truncated && mv -- "$file".truncated exported/"${file%.png*}.png"
            echo "  exported $file" >&2
        fi
    done

    wait
    echo "Done!" >&2
    exit 0
fi

echo "Exiting script." >&2
exit 0
